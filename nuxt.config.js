

export default {
	ssr: false,
	head                : {
		htmlAttrs : { lang: 'en' },
		meta      : [ { charset: 'utf-8' }, { name: 'viewport', content: 'width=device-width, initial-scale=1' },{ hid: 'description', name: 'description', content: 'Content PWA' }, { name: 'theme-color', content: '#3d5191' } ],
		link      : [ { rel: 'icon', type: 'image/x-icon', href: '/favicon.png' } ]
	},

	// Target (https://go.nuxtjs.dev/config-target)
	target              : 'static',

	// Global CSS (https://go.nuxtjs.dev/config-css)
	css                 : [ '~assets/scss/colors.scss' ],

	// Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
	plugins             : [ '~/plugins/v-img.js', {src:"~/plugins/pwa-update.js", mode: "client"} ],
	// Auto import components (https://go.nuxtjs.dev/config-components)
	components          : true,

	// Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
	buildModules        : [ '@aceforth/nuxt-optimized-images' ],

	optimizedImages     : {
		optimizeImages : true
	},

	publicRuntimeConfig : {
		baseURL : process.env.BASE_URL
	},

	generate            : {
		fallback : true
	},

	image               : {
		// Options
	},

	// Modules (https://go.nuxtjs.dev/config-modules)
	modules             : [ '@nuxtjs/bulma', '@nuxt/content', '@nuxt/image', '@nuxtjs/style-resources', '@nuxtjs/pwa' ],

	content             : {
		markdown : {
			prism : {
				// https://github.com/PrismJS/prism-themes
				theme : 'prism-themes/themes/prism-atom-dark.css'
			}
		}
	},

	styleResources      : {
		scss : [ './assets/scss/*.scss' ]
	},
	pwa: {
        manifest: {
            name: "Roblog",
            lang: "en",
            display: "standalone",
            description: "Chronicles",
	    theme_color: '#3d5191',
        }
    },

	// Build Configuration (https://go.nuxtjs.dev/config-build)
	build: {
		postcss : {
			preset : {
				features : {
					customProperties : false
				}
			}
		},
		html: {
			minify:{
				minifyCSS: true,
				minifyJS: true
			}
		}
	}
};
