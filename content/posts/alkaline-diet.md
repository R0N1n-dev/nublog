---
title: Alkaline diet
description: Is this diet even for real.
img: cats/header.png
img_alt: A nice food display.
date: 2022-05-21 # Date should always be in YYYY-MM-DD format
---

## The Alkaline diet

As a nutritionist, I often get the question about diets, mostly pertaining to weight loss. It normally initiates an interesting conversation that doesn't go far as most people think they actaually know more than they do. They litetrally try to eduacate you based on some "knowledge" that they read online.

<b>
Not everything you read on the internet is true

</b>
<br></br>

The Alkaline diet is based on what we call a "nutriiton fad". 
This is a piece of information about foods and nutritional uses  based on false information or biased understading of the science behind the food.

I mainly found the alkaline diet to be of particular interest because a large number of people have been drawn into this diet due to the increasing numbers of non communicable diseases, especially cancer which it claims to be particularly good at improving.

This diet and its proponents lean on the premise of "changing the body's PH to more alkaline as cancer cells prefer acidic enviroments".

Firts of all, you <b>CANNOT dictate your body's pH</b>.
The human body is a well-maintained system. You cannot mess with it or else <b>YOU DIE</b>
.
The human body is slightly alkaline (ph = 7.4) but that is regularly regulated. You cannot change that value without consequence to the rest of the body. That's just how it is


However, the pH of urine and saliva can be changed
